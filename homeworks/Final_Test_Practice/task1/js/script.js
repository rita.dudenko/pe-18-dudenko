const str = "Профессионал — не тот, кто в совершенстве владеет техникой ремесла (с этим-то у него как раз полный " +
    "порядок), а тот, кто, получив задание, всегда выдает конечный результат, невзирая ни на какие привходящие моменты… " +
    "Ибо профессионал отличается от любителя еще и тем, что играет не до забитого им красивого гола и не до своего " +
    "«психологического кризиса», а до шестидесятой секунды последней минуты матча.";

const substr = 'ВСегда';

let count = 0;

 function getSubstrCount (someStr, someSubstr, flag = true){
    let currentString;
    let currentSubstring;

    if (flag){
        currentString = someStr.toLowerCase();
        currentSubstring = someSubstr.toLowerCase();
    } else {
        currentString = someStr;
        currentSubstring = someSubstr;
    }


   let pos = currentString.indexOf(currentSubstring);

   while ( pos >= 0 ){
       count ++;
       pos =  currentString.indexOf(currentSubstring, pos + 1);

   }

   console.log(count);



 }

getSubstrCount(str, substr);

