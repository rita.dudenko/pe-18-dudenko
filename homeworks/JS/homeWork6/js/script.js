const userArray = ['hello', 'world', 23, '23', null, {}, [4,5,2], function () {

}, true, false, true, null, undefined];



function filterBy(arr, type) {

     return arr.filter(item =>
         (item === null) ? type !== 'null' : typeof item !== type);

}

console.log(filterBy(userArray, 'null'));

