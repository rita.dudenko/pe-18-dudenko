function createNewUser() {
    let userName = prompt('Enter your first name', '');
    let userLastName = prompt('Enter your second name', '');
    let userBirthday = prompt('Enter your birthday', 'dd.mm.yyyy');
    const data = userBirthday.split('.');

    const newUser = {
        firstName: userName,
        lastName: userLastName,
        birthday: userBirthday,

        getAge() {
            let currentDate = new Date();
            let monthDif = currentDate.getMonth() + 1 - data[1];
            let dayDif = currentDate.getDate() - data[0];
            let yearDif = currentDate.getFullYear() - data[2];

            if (monthDif < 0) {
                return yearDif - 1;

            }else if (monthDif === 0) {
                return  (dayDif >= 0) ? yearDif : yearDif - 1;

            }else return yearDif;
        },

        getLogin() {

            return (this.firstName[0] + this.lastName).toLowerCase();
        },

        getPassword() {

            return (this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + data[2]);
        }

    };

    return newUser;

}

const currentUser = createNewUser();

console.log(currentUser);
console.log(`${currentUser.firstName} ${currentUser.lastName} ${currentUser.birthday} -> ${currentUser.getPassword()}`);
console.log(`You are  ${currentUser.getAge()} year old`);
























