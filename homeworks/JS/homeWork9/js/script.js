
let selectedTab;
const tabs = document.getElementsByClassName("tabs");
const tabsLi = tabs[0].children;

tabs[0].addEventListener("click", onTabClicked);




function onTabClicked(event) {
    let tab = event.target;
    if (tab.tagName !== "LI"){
        return;
    }

    showTab(tab);


}


function showTab(currentTab) {
    tabsLi[0].classList.remove("active");
    document.getElementById(`${tabsLi[0].dataset.tabText}`).classList.remove("visible");
    document.getElementById(`${tabsLi[0].dataset.tabText}`).classList.add("hidden");


    if (selectedTab){
        selectedTab.classList.remove("active");
        document.getElementById(`${selectedTab.dataset.tabText}`).classList.remove("visible");
        document.getElementById(`${selectedTab.dataset.tabText}`).classList.add("hidden");

    }
        selectedTab = currentTab;

    selectedTab.classList.add("active");
    document.getElementById(`${selectedTab.dataset.tabText}`).classList.remove("hidden");
    document.getElementById(`${selectedTab.dataset.tabText}`).classList.add("visible");

}






