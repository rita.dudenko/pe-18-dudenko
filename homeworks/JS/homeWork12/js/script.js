const images = document.querySelectorAll('.image-to-show');
const divElement = document.querySelector('.images-wrapper');
const stopButton = document.createElement('button');
const continueButton = document.createElement('button');
const backTimerDiv = document.createElement('div');

addAfterDOMElement(backTimerDiv, ' ', 'back_timer_wrapper', divElement);
addAfterDOMElement(stopButton, 'Stop', 'btn', backTimerDiv);
addAfterDOMElement(continueButton, 'Continue', 'btn', stopButton);

stopButton.addEventListener('click', stopShow);
continueButton.addEventListener('click', continueShow);

const firstInitTime = new Date().getTime() + 10000;
backTimer(firstInitTime);

let timerId = setInterval( showImageWithTimer,10000);


 function showImageWithTimer ()  {
     const initialTime = new Date().getTime() + 10000;

     backTimer(initialTime);
     showImage();

}


let i = 1;

function showImage(){

    const visibleImg = document.querySelector('.visible');
    if (visibleImg){

        visibleImg.classList.replace('visible', 'hidden' );
    }

    if(i >= images.length){
        i = 0;
    }


    // requestAnimationFrame(function (currentTime) {
    //     start = new Date().getTime();
    //     fadeIn(currentTime, images[i], 500);
    //
    // });

    images[i].classList.replace('hidden', 'visible');
    i++;

}


function addAfterDOMElement(element, inner, className,  prevNode) {
    element.innerHTML = inner;
    element.classList.add(className);
    prevNode.after(element);
}

function stopShow() {
    clearInterval(timerId);

}

 function continueShow() {
  timerId = setInterval( showImageWithTimer,10000);

 }



function backTimer(time) {                            //подумать как остановить время при остановке картинки!!!!
    const currentTime = new Date().getTime();

    if (currentTime < time){
        backTimerDiv.innerHTML = `00:0${new Date(time - currentTime).getSeconds()}:
                                         ${new Date(time - currentTime).getMilliseconds()}`;
        setTimeout(function (){
            backTimer(time);
        });

    }else backTimerDiv.innerHTML = '00:10:00';

}



// let start;
//
//  function fadeIn(currentTime, element, duration) {
//
//      currentTime = new Date().getTime();
//      let runtime = currentTime - start;
//      element.style.opacity = `${element.getComputedStyle().opacity + runtime / duration}`;
//
//      if (runtime < duration) { // if duration not met yet
//          requestAnimationFrame(function (currentTime) { // call requestAnimationFrame again with parameters
//              fadeIn(currentTime, element, duration);
//          })
//      }
//
//  }









