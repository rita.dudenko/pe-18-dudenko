
const customDOM = {
     createElement: tagName => document.createElement(tagName),
     append: (parent, child) => parent.appendChild(child),
     pageClearing: (timeOut) =>  setTimeout(() => document.body.innerHTML = '', timeOut)

};

