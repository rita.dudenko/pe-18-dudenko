const arr = ['hello', 'from', 'wonderful', 'cities:', ['Kiev', ['Kharkiv', 'Odessa'], 'Lviv'], 'welcome'];



function showList(someArray){
    return  someArray.map((item, i, someArray) =>
        Array.isArray(item) ? `<li><ul>${showList(someArray[i]).join('')}</ul></li>` : `<li>${item}</li>`
    );

}

function addList(currentArray){
    let ul = customDOM.createElement('ul');
    customDOM.append(document.body, ul);

    ul.innerHTML = showList(currentArray).join('');

}



const time = new Date().getTime() + 10000;

function backTimer() {
    const currentTime = new Date().getTime();
    let timeArr = new Date(time - currentTime).toUTCString().split(' ');

    if (currentTime < time){
        divTimer.innerHTML = timeArr[4];
        setTimeout(backTimer, 1000);

    }else divTimer.innerHTML = '00:00:00';

}


let divTimer = customDOM.createElement('div');
customDOM.append(document.body, divTimer);


addList(arr);
backTimer();
customDOM.pageClearing(10000);