document.addEventListener("DOMContentLoaded", onDocumentLoaded);
const input = document.getElementById("price_input");


function onDocumentLoaded() {
    input.addEventListener("focus", onInputFocused);
    input.addEventListener("blur", onInputBlurred);
}


function onInputFocused() {
    if (input.classList.contains("incorrect_data_input")){
        input.classList.remove("incorrect_data_input");
        const error = document.getElementById("error_message");
        error.innerHTML = "";

    }

    if (input.classList.contains("blurred_input")){
        input.classList.remove("blurred_input");
        input.value = "";
    }

    input.classList.add("focused_input");
}


function onInputBlurred() {

    input.classList.add("blurred_input");


    if (input.value === ""){
        return;
    }

    if (input.value < 0){
        input.classList.add("incorrect_data_input");
        const message = document.createElement("p");
        message.setAttribute("id", "error_message");
        document.body.appendChild(message);
        message.innerText = "Please enter correct price!";
        return;

    }

    const div = document.createElement("div");
    const span = document.createElement("span");
    const button = document.createElement("button");
    const priceDiv = document.getElementById("price_div");
    priceDiv.before(div);
    div.appendChild(span);
    div.appendChild(button);

    div.classList.add("span_wrapper");
    button.classList.add("button_class");

    span.innerText = `Current price: ${input.value}`;
    button.innerText = "x";

    button.addEventListener("click", function () {
        div.remove();
         input.value = "";
    });

}


















