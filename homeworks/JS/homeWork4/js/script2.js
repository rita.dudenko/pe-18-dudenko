function createNewUser() {

    let  userName = prompt('Enter your first name', '');
    let  userLastName = prompt('Enter your second name', '');

    const newUser = {

        setFirstName(){

            Object.defineProperty(this, 'firstName', {
                value: userName

            });

        },

        setLastName(){

            Object.defineProperty(this, 'lastName', {
                value: userLastName

            });
        },

        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase();
        }
    };

    return newUser;

}


const currentUser =  createNewUser();

currentUser.setFirstName();
currentUser.setLastName();

console.log (`${currentUser.firstName} ${currentUser.lastName} --> ${currentUser.getLogin()}`);

