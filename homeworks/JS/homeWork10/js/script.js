const button = document.querySelector(".btn");
const inputCollection = document.querySelectorAll("input");
button.addEventListener("click", inputValidation);

inputCollection.forEach(input => input.addEventListener("focus", onInputFocused));


const icons = document.querySelectorAll(".icon-password");

icons.forEach((icon, index) => icon
                                                     .addEventListener("click", function (){
                                                                               changeIcon(icon, inputCollection[index])
                                                                                }

));


function changeIcon(currentIcon, currentInput) {
    currentIcon.classList.toggle("fa-eye");
    currentIcon.classList.toggle("fa-eye-slash");
    (currentIcon.classList.contains("fa-eye-slash")) ? currentInput.setAttribute('type', "text") :
        currentInput.setAttribute('type', "password");
}




function inputValidation() {
    let message;

    messageClear();

    if (!inputCollection[0].value && !inputCollection[1].value) {

        messageDOMCreation(message, button, 'You must fill all fields!');
        event.preventDefault();
        return;
    }

    if (inputCollection[0].value === inputCollection[1].value) {
        alert('You are welcome!');
        event.preventDefault();
        return;
    }

        messageDOMCreation(message, button, 'You must enter equal values!');
    event.preventDefault();

}


function messageDOMCreation(someMessage,prevEl, text) {
    someMessage = document.createElement("span");
    prevEl.before(someMessage);
    someMessage.classList.add("error-message");
    someMessage.innerHTML = text;
}

function onInputFocused() {
    messageClear();

}

function messageClear() {
    let  errorMessage = document.querySelector(".error-message");
    if (errorMessage){
        errorMessage.remove();
    }
}


