const currentArray = [1, 2, [3,4,[5]], { name: 'Vasya',
                                         age:24,
                                         job: { department: 'IT',
                                                 region: 'Kiev'},
                                         car: { name: 'Audi',
                                                model:'Q8',
                                                region: 'German'
    }}];

function cloneObject(someObject) {
    let newObject;

    if (Array.isArray(someObject)){
        newObject = [];
        someObject.forEach((item, index) =>
             (item === null || typeof (item) !== 'object') ? newObject[index] = item :
                 newObject[index] = cloneObject(item));


    }else if (typeof(someObject) === 'object'){
        newObject = {};
        for (let key in someObject){

            (typeof(someObject[key]) !== 'object') ? newObject[key] = someObject[key] :
                newObject[key] = cloneObject(someObject[key]);

        }
    }

   return newObject;

}

console.log(cloneObject(currentArray));

