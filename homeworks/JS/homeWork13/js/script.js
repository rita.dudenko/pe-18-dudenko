let link = document.createElement('link');
 document.head.appendChild(link);
 link.rel = 'stylesheet';

themeInstallation();


const tabs = document.getElementsByClassName('tabs');
const div = document.querySelector('.centered-content');
const button = document.createElement('button');


div.after(button);
button.classList.add('btn');
button.innerText = 'Change theme';


tabs[0].addEventListener('click', onTabClicked);
button.addEventListener('click', onThemeChanged);



function themeInstallation() {
    if (localStorage.getItem('themeStyle') === 'dark') {
        link.href = 'css/dark.css';

    } else {
        link.href = 'css/beige.css';
    }

}


function onTabClicked(event) {
    let tab = event.target;
    if (tab.tagName !== "LI") {
        return;
    }

    showTab(tab);


}


function showTab(currentTab) {
    const prevTab = document.querySelector('.active');
    prevTab.classList.remove('active');


    document.getElementById(`${prevTab.dataset.tabText}`).classList
        .replace('visible', 'hidden');


    currentTab.classList.add('active');
    document.getElementById(`${currentTab.dataset.tabText}`).classList
        .replace('hidden', 'visible');

}


function onThemeChanged() {

    if (link.href.includes('css/dark.css')) {
        link.href = 'css/beige.css';
        localStorage.setItem('themeStyle', 'beige');
    } else {
        link.href = 'css/dark.css';
        localStorage.setItem('themeStyle', 'dark');
    }


}








