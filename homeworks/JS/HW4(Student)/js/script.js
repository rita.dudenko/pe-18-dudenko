function getStudent() {
    const student = {
        name: null,
        lastname: null,
        table: {}
    };

    const studentName = prompt ('Enter your name', '');
    const studentLastName = prompt('Enter your last name', '');

    student.name = studentName;
    student.lastname = studentLastName;

    return student;

}


function isIncorrectMarkValid(studentMark) {
    if (isNaN(+studentMark) || studentMark < 0 || studentMark > 12) {
        alert('Incorrect mark ');
        return true;
    }
}

function isPropInObject(prop, someObject) {

    if (prop in someObject) {
        alert('Such item already exists. Your mark will be rewrite');
        return true;
    }
}



 function fillTable(student){

 let lesson = prompt('Enter your lesson', 'math, literature, biology etc.');
 let mark = prompt('Enter your mark', 10);

     while (lesson !== null && mark !== null) {

        if (isIncorrectMarkValid(mark)){
             mark = prompt('Enter your mark', mark);
         }

        isPropInObject(lesson, student.table);

        student.table[lesson] = +mark;

         lesson = prompt('Enter your lesson', 'math, literature, biology etc.');
         if (lesson === null) break;

         mark = prompt('Enter your mark', 10);

     }
 }


function studentAnalize(student) {

    let badMarkCount = 0;                   // счетчик плохих оценок
    let markSum = 0;                        // сума всех оценок
    let markCount = 0;                     // счетчик всех оченок

    for (let i in student.table) {

        if (student.table[i] < 4) {
            badMarkCount++;
        }

        markCount++;
        markSum += student.table[i];
    }

    if (badMarkCount === 0 ) {
        console.log('Student is transferred to the next course');

        if ((markSum / markCount) > 7) {
            console.log('Student has scholarship');
        }

    }
    return (`The average mark: ${markSum / markCount}`);
}

const currentStudent = getStudent();
fillTable(currentStudent);
 console.log(studentAnalize(currentStudent));


