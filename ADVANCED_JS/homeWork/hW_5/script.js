let result = 0;

function sum(a){
    if (a === undefined) return result;
    result += a;

    return sum;

}

const r = sum(1)(2)(3)(4)()
console.log('--->', r);
