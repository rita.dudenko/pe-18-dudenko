const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

const div = document.querySelector('#root');


const showBookList  = (outerElement, bookList) => {
    const ul = document.createElement('ul');
    outerElement.insertAdjacentElement('beforeend',ul);

    // bookList.forEach(({author, name, price}, index) => {
    //     try {
    //         if (!author)
    //             throw new Error(`There is no author in book ${++index}`);
    //
    //         if (!name)
    //             throw new Error(`There is no title in book ${++index}`);
    //
    //         if (!price)
    //             throw new Error(`There is no price in book ${++index}`);
    //
    //         const li = document.createElement('li');
    //         ul.insertAdjacentElement('beforeend',li);
    //         li.innerText = `${author}, "${name}" - ${price}`;
    //
    //
    //     }catch (err) {
    //         console.log(err.message);
    //
    //     }

    //});

   const finalList = bookList.reduce((prev, {author, name, price}, index) => {
        try {
            if (!author)
                throw new Error(`There is no author in book ${++index}`);

            if (!name)
                throw new Error(`There is no title in book ${++index}`);

            if (!price)
                throw new Error(`There is no price in book ${++index}`);

            return [...prev, `<li>${author}, "${name}" - ${price}</li>`];

        }catch (err) {
            console.log(err.message);
            return prev;

        }


    },[]);

   ul.innerHTML = finalList.join('');

};

showBookList(div, books);

