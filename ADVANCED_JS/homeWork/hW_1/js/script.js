
//*** Task 1

const cityArr = ["Kyiv", "Berlin", "Dubai", "Moscow", "Paris"];

const showCityList = (currentArr) => {
    [city1, city2, city3, city4, city5] = currentArr;
    console.log(city1, city2, city3, city4, city5);
};

showCityList(cityArr);


//*** Task 2

const Employee = {
    name: "Ivan",
    salary: "1000USD"
};

const showSalary = (currentObject) => {
    const {name, salary} = currentObject;
    console.log(`${name}:  ${salary}`);

};

showSalary(Employee);


//*** Task 3

const array = ['value', 'showValue'];

const showArrElements = (currentArray) => {
    const [value, showValue] = currentArray;
    alert(value);
    alert(showValue);

};

showArrElements(array);