const CURRENCY = Object.freeze({
   DOLLAR: "USD",
    EURO: "EUR",
    GRIVNA: "UAH"
});


class Employee {
    #name;
    #age;
    #salary;
    constructor({name, age, salary}) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    set name(name){
        if(!name ){
            throw new Error('Enter name');
        }
        this.#name = name;

    }

    get name() {
        return this.#name;
    }

    set age(age){
        if(!age || isNaN(age)){
            throw new Error('Enter correct age');
        }
        this.#age = parseInt(age);

    }

    get age() {
        return this.#age;

    }

    set salary(salary) {
        if(!salary || isNaN(salary) || salary < 0){
            throw new Error('Enter correct salary');
        }
        this.#salary = parseFloat(salary);
    }

    get salary() {
        return [this.#salary,CURRENCY.DOLLAR].join('');
    }

}

class Programmer extends Employee{
    constructor({lang,...params}) {
        super(params);
        this.lang = lang;
    }


    set salary(salary) {
        super.salary = salary;
    }

    get salary() {
        const newSalary = 3 * parseFloat(super.salary);
        return [newSalary,CURRENCY.DOLLAR].join('');
    }

}

const programmer1 = new Programmer({name:'Ivan',
                                   age: 20,
                                   salary: 1000,
                                   lang: ['CSS3', 'HTML5','Java', 'C#', 'JavaScript'],
                                   });

const programmer2 = new Programmer({name:'Oleh',
                                    age: 24,
                                    salary: 1200,
                                    lang: ['CSS3', 'HTML5','Java', 'C#', 'JavaScript','Python'],
                                   });
const programmer3 = new Programmer({name:'Anna',
                                    age: 20,
                                    salary: 1400,
                                    lang: ['CSS3', 'HTML5','Java', 'C#', 'JavaScript','React', 'Ruby'],
                                    });

console.log(programmer1);
console.log(programmer2);
console.log(programmer3);
