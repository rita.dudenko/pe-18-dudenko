const button = document.getElementById('button');
button.addEventListener('click', showAdress, {once: true});

const fetchUrl = (url) => {
  return   fetch(url)
        .then(response => {
            if (response.ok) {
                return response.json()
            }

            throw new Error();

        })
};


function showAdress() {
    fetchUrl('https://api.ipify.org/?format=json')
        .then(({ip}) => {
            fetchUrl(`http://ip-api.com/json/${ip}?fields=country,regionName,city,timezone`)
                .then(data => {
                    showElements(data);
                })
        })
        .catch(error => console.log(error.message));

}



 function showElements(currentObject) {
     const container = document.querySelector('.container');
     for (key in currentObject){
         const fragment = new DocumentFragment();
         const title = document.createElement('h3');
         title.innerHTML = key;
         fragment.append(title);
         const context = document.createElement('p');
         context.innerText = currentObject[key];
         fragment.append(context);
         container.append(fragment);
     }

 }