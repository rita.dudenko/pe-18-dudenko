class Table{
     activeCell;

    constructor() {
        this.table = document.querySelector('table');
        this.tdList =this.table.querySelectorAll('td');

    }

    get greenCells(){
        return document.querySelectorAll('.user-win').length;
    }

    getRedCells(){
        return document.querySelectorAll('.computer-win').length;
    }


    activateCell(array){
        array.item(Math.round(Math.random()*100)).classList.add('active');
    };

    deactivateCell(){
        this.activeCell = document.querySelector('.active');
        if(!(this.activeCell.classList.contains('user-win'))){
            this.activeCell.classList.add('computer-win');
        }
        this.activeCell.classList.remove('active');
    };

}


class Player{
    constructor(name) {
        this.name = name;
        this.points = 0;

    }

     #personalScore() {

    }

     isCatched(cell){
         cell.addEventListener('click',function (event) {
             if(event.target.classList.contains('active')){
                 event.target.classList.add('user-win');
             }
         });
    }

    addPoint() {}
}


class Game {
    #currentInterval = null;
    constructor() {
        this.playerHuman = new Player('serega');
        this.playerPC = new Player('serega2');
        this.gameField = new Table();
    }

    #clearInterval(){
        clearInterval(this.#currentInterval);
        this.#currentInterval = null;
    }

    catchCell(){
        this.player.isCatched(this.gameField.table);
        setTimeout(() => {
            this.gameField.deactivateCell();
        },1450);

    }

    checkOnPush() {
        // ....timeout
        if(true) {    //remove inside if
            this.playerHuman.addPoint();
        } else {
            this.playerPC.addPoint();
        }

        this.checkOnFInish()
    }

    checkOnFInish() {

    }

    highlightCell(){
       this.#currentInterval = setInterval(() => {
            this.gameField.activateCell(this.gameField.tdList);
            this.catchCell();
            this.finish();
            },1500);
    }



    finish(){
        const playerScore = this.gameField.getGreenCells();
        const computerScore = this.gameField.getRedCells();
        console.log(playerScore);
        console.log(computerScore);


        if( playerScore > this.gameField.tdList.length / 12){
            this.showResults('You win! Tadadada!!!');
            return this.#clearInterval();
        }

        if(computerScore > this.gameField.tdList.length / 12){
            this.showResults('You lose! Kvakvakvakva!!!');
            return this.#clearInterval();
        }

    }


    showResults(message){
        const modal = document.getElementById('modal');
        modal.innerText = message;
        modal.classList.remove('modal-window-invisible');
    }


}

// init game
//        init 2 players
//        init 1 table
//        [1....100] => lodash shuffle

//          timer game - like interval
//                parent===child
//          timer inner - for user to push the TD
//              set listener => only for the TD (don't forget to remove it afterwards)
//

const game = new Game();
game.highlightCell();

