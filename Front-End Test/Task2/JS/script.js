$(document).ready (function () {
    $('.table').on('click', 'td', function (event) {

        hideElement($('.table-header'));
        hideElement( $('.table-body-client__phone'));
        hideElement( $('.table-body-client__email'));
        hideElement($('.table-body-client__skype'));
        hideElement($('.table-body-client__comments'));

        $('.table').addClass('short-table');
        $('.table-body-client-fio').addClass('table-body-client-fio--less-padding');

        showElement($('.client-add-info'));

        addClientData();

        event.preventDefault();
    });

    function hideElement(element){
        element.addClass('hidden');

    }

    function showElement(element){
        element.removeClass('hidden');

    }

    function addClientData() {
        $('.table-body-client__fio').each(function () {
            if (($(this).children().length === 1)) {
                const email = $(this).siblings('.table-body-client__email').html();
                const phone = $(this).siblings('.table-body-client__phone').html();

                $(this).append($(`<div class="table-body-client__short-table-email">${email}</div>
                                <div class="table-body-client__short-table-phone">${phone}</div>`));
            }

        });
    }


    $('.table-body-client').click( function () {
        $('.table-body-client').removeClass('active');
        $(this).addClass('active');
        $('.client-add-info-content').html('');
        const clientId = $(this).attr('id');
        showAddInformation(clientId, clientsAddInfo);

    });






    const clientsAddInfo = [
        {
            id: '1',
            date: '21.05.2018',
            age: 24,
            married: 'замужем',
            work: 'Epam, developer',
            school: 'Высшее, НТУУ КПИ, факультет информатики'
        },

        {
            id: '2',
            date: '20.11.2018',
            age: 25,
            married: 'холост',
            work: 'Global Logic, UI',
            school: 'Высшее, НТУУ КПИ, факультет информатики'
        },

        {
            id: '3',
            date: '05.03.2019',
            age: 28,
            married: 'не замужем',
            work: 'Luxoft, QA',
            school: 'Высшее, НаУКМА, факультет информатики'
        }

    ];

    function showAddInformation(clientId, clientsArr) {
        const clientInfo = clientsArr.find(client => client.id === clientId);
        const {date, age, married, work, school} = clientInfo;

        $('.client-add-info').prepend($('<div class="client-add-info__content client-add-info-content"></div>')
            .html($(`<div class="client-add-info-content__date">Добавлен(на): ${date}</div>
                   <div class="client-add-info-content__age">Возраст: ${age}</div>
                   <div class="client-add-info-content__married">Семейное положение: ${married}</div>
                   <div class="aclient-add-info-content__work">Работает: ${work}</div>
                   <div class="client-add-info-content__school">Образование: ${school}</div>`)));
    }



    $('.client-add-info__close').click(function () {
        $('.table').removeClass('short-table');
        hideElement($('.client-add-info'));
        showElement($('.table-header'));
        showElement($('.table-body-client__phone'));
        showElement($('.table-body-client__email'));
        showElement($('.table-body-client__skype'));
        showElement($('.table-body-client__comments'));

        $('.table-body-client').removeClass('active');
        $('.table-body-client-fio').removeClass('table-body-client-fio--less-padding');

            $('.table-body-client__short-table-email').remove();
            $('.table-body-client__short-table-phone').remove();


    });

 });