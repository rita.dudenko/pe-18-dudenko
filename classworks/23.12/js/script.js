const userYearOfBirth = Number(prompt('Введите год рождения'));
const userMonthOfBirth = Number(prompt('Введите месяц рождения'));
const userDayOfBirth = Number(prompt('Введите день рождения'));
const currentYear = 2019;
const currentMonth = 12;
const currentDay = 23;


if (isNaN(userYearOfBirth) || userYearOfBirth === 0 || isNaN(userMonthOfBirth) || userMonthOfBirth === 0 ||
    isNaN(userDayOfBirth) || userDayOfBirth === 0 ){
    alert ('Введите правильно!');
    throw new Error('Ошибка ввода');

}



let resultYear;
let resultDay;
let resultMonth;


if ( currentMonth >= userMonthOfBirth && currentDay >= userDayOfBirth){
    resultYear = currentYear - userYearOfBirth;
    resultMonth = currentMonth - userMonthOfBirth;
    resultDay = currentDay - userDayOfBirth;
    alert(resultYear + '.' +  resultMonth + '.' + resultDay );

}

else if ( currentMonth >= userMonthOfBirth && currentDay < userDayOfBirth){
    resultYear = currentYear - userYearOfBirth - 1;
    resultMonth = currentMonth + 12 - userMonthOfBirth;
    resultDay = currentDay - userDayOfBirth;
    alert(resultYear + '.' +  resultMonth + '.' + resultDay );

}

