const customDOM = {

    getAllElements: () => document.getElementsByTagName('*'),
    getById: (id) => document.getElementById(id),
    createElement: (tagName) => document.createElement(tagName),
    append: (parent,child) => parent.appendChild(child)

};
