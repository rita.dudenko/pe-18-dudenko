const http = require('http');

const host = 'localhost';

const port = process.argv[2]?.split('=')[1] || '3000';

let reqCount = 0;

const requestListener = (req, res) => {
  reqCount += 1;
  res.writeHead(200);
  res.end(JSON.stringify({
    message: 'Request handled successfully',
    requestCount: reqCount,
  }));
};

const server = http.createServer(requestListener);

server.listen(port, host, () => {
  console.log(`Server is running on https://${host}:${port}`);
});
