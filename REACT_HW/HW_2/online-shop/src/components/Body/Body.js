import React, {PureComponent} from 'react';
import './Body.scss';
import Card from "../Card/Card";
import PropTypes from 'prop-types';


class Body extends PureComponent {


    render() {
        const {products, addToFav, removeFav, openModal, getCurrentProduct, favourites, closeCart} = this.props;
        const productsList = products.map(product =>
            <Card key={product.id}  product={product} addToFav={addToFav} removeFav={removeFav}
                  openModal={openModal}  getCurrentProduct={getCurrentProduct} favourites={favourites}/>);

        return (
            <div className='body' onClick={closeCart}>
                <h1 className='title'>Magic <span className='title-span'>Garden</span></h1>
                <h2 className='subtitle'>Мы создали Magic Garden для того, чтобы вам не пришлось думать, как лучше выразить свои чувства.</h2>
                {productsList}
            </div>
        );
    }
}

Body.propTypes = {
    products: PropTypes.array.isRequired,
    addToFav: PropTypes.func,
    removeFav: PropTypes.func,
    openModal: PropTypes.func.isRequired,
    getCurrentProduct: PropTypes.func.isRequired,
    favourites: PropTypes.array,
    closeCart: PropTypes.func,

};

Body.defaultTypes = {
    addToFav: undefined,
    removeFav: undefined,
    closeCart: undefined,
};

export default Body;