import React, {PureComponent} from 'react';
import './Header.scss';
import Button from "../Button/Button";
import Icon from "../Icon/Icon";
import Cart from "../Cart/Cart";
import PropTypes from 'prop-types';



class Header extends PureComponent {


    render() {
        const {cartList, removeFromCart, closeCart, cartIsOpen, doShopping} = this.props;
        const productAmount = !!cartList.length ? `(${cartList.length})` : '';

        return (
            <div className='header' onClick={closeCart}>
                <div className='header-container'>
                    <div className='icons-block'>
                        <Icon className='fa fa-facebook-square  '/>
                        <Icon className='fa fa-twitter-square '/>
                        <Icon className='fa fa-instagram'/>
                        <Icon className='fa fa-pinterest-square'/>

                    </div>
                    <div className='cart-wrapper'>
                        <Button backgroundColor='indianred' className='btn--cart' onClick={this.showCart}>
                            <Icon className='fa fa-shopping-cart icon--white'/> Cart <span>{productAmount}</span>
                        </Button>
                        {cartIsOpen && <Cart cartList={cartList} closeCart={closeCart}
                                             removeFromCart={removeFromCart}  doShopping={doShopping}/>}
                    </div>

                </div>
            </div>
        );

    }

    showCart = (e) => {
        const {showCart} = this.props;
        showCart();
        e.stopPropagation();
    }
}

Header.propTypes = {
    cartList: PropTypes.array,
    removeFromCart: PropTypes.func,
    showCart: PropTypes.func,
    closeCart: PropTypes.func,
    cartIsOpen: PropTypes.bool,
    doShopping: PropTypes.func,
};

Header.defaultProps = {
    removeFromCart: undefined,
    showCart: undefined,
    closeCart: undefined,
    doShopping: undefined,
};



export default Header;