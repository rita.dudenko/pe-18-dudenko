import React, {PureComponent} from 'react';
import "./Card.scss";
import Button from "../Button/Button";
import Icon from "../Icon/Icon";
import PropTypes from 'prop-types';

class Card extends PureComponent {
    render() {
        const {product, favourites} = this.props;
        const {title, price, url, article, color} = product;
        const isInFav = favourites.find(item => item === product.id);

        return (
            <div className='card'>
                <div className="img-wrapper"><img src={url} alt='flower'/></div>
                <h5 className="product-title">{title}</h5>
                <p className="product-article">Артикул: {article}</p>
                <p className="product-color">Цвет: {color}</p>
                <h5 className="price">{price}
                    {!isInFav && <Icon className="fa fa-star-o icon--m-l-30px" onClick={this.addCardToFav}/>}
                    {!!isInFav && <Icon className="fa fa-star icon--gold icon--m-l-30px" onClick={this.removeFromFav}/>}
                </h5>
                <div className='button-wrapper'>
                    <Button backgroundColor='indianred' className='btn--card' text="Купить"
                            onClick={this.addProductToCart}/>
                </div>
            </div>
        );
    }

    addCardToFav = () => {
        const {product, addToFav} = this.props;
         addToFav(product.id);
    };

    removeFromFav = () => {
        const {product, removeFav} = this.props;
        removeFav(product.id);
    };

    addProductToCart = () => {
        const {product, openModal, getCurrentProduct}  = this.props;
        openModal();
        getCurrentProduct(product);

    }

}

Card.propTypes = {
    product: PropTypes.shape({
        id: PropTypes.number.isRequired,
        title: PropTypes.string.isRequired,
        price: PropTypes.string.isRequired,
    }).isRequired,
    favourites: PropTypes.array,
    addToFav: PropTypes.func,
    removeFav: PropTypes.func,
    openModal: PropTypes.func.isRequired,
    getCurrentProduct: PropTypes.func.isRequired,
};

Card.defaultProps = {
    addToFav: undefined,
    removeFav: undefined,
};

export default Card;