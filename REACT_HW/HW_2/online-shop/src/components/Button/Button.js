import React, {PureComponent} from 'react';
import './Button.scss';
import PropTypes from 'prop-types';

class Button extends PureComponent {

    render() {
        const {backgroundColor, text, onClick, className, children } = this.props;
        const btnClassName = className ? `btn ${className}` : 'btn';
        return (
            <button style={{backgroundColor}} className={btnClassName}  onClick={onClick}>{text}{children}</button>

        );
    }
}

Button.propTypes = {
    backgroundColor: PropTypes.string,
    text: PropTypes.string,
    onClick: PropTypes.func,
    className: PropTypes.string,
    children: PropTypes.array,
};

Button.defaultProps = {
    text: '',
    backgroundColor: 'lightgrey',
    className: '',
    children: [],
    onClick: undefined,
};

export default Button;