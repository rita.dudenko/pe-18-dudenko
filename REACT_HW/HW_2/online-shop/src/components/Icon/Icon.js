import React, {PureComponent} from 'react';
import 'font-awesome/css/font-awesome.min.css';
import './Icon.scss';
import PropTypes from 'prop-types';

class Icon extends PureComponent {
    render() {
        const {className, onClick} = this.props;
        return (
            <i className={`${className} icon`} onClick={onClick} aria-hidden="true"/>
        );
    }
}

Icon.propTypes = {
    className: PropTypes.string.isRequired,
    onClick: PropTypes.func,

};

Icon.defaultProps = {
    onClick: undefined,
};

export default Icon;