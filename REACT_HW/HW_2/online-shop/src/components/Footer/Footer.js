import React, {PureComponent} from 'react';
import './Footer.scss';

class Footer extends PureComponent {
    render() {
        return (
            <div className='footer'>
                <p className="footer-content">&copy;Magic Garden. License since 2017</p>

            </div>
        );
    }
}

export default Footer;