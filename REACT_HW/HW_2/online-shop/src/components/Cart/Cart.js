import React, {PureComponent} from 'react';
import './Cart.scss'
import Button from "../Button/Button";
import Icon from "../Icon/Icon";
import PropTypes from 'prop-types';

class Cart extends PureComponent {
    render() {
       const {cartList, closeCart, removeFromCart, doShopping} = this.props;

       const chosenProducts = cartList.map(item => {
           const {id,title, url, price} = item;
           return(
               <div className='product-cart-view' key={id}>
                   <span><img className='cart-img' src={url} alt='flower'/></span>
                   <span className='product-cart-title'>{title}</span>
                   <span className='product-cart-price'>{price}</span>
                   <span className='delete-product' onClick={() => removeFromCart(id)}><Icon className='fa fa-times'/></span>
               </div>)
       });

       const totalAmount = cartList.reduce((sum, product) => {
          const {price} = product;
          return sum + parseInt(price);
       } ,0);

        return (
            <div className='cart-container' onClick={e => e.stopPropagation()}>
                {chosenProducts}
                {!!cartList.length &&  <div className="total-sum">
                                        <span>К оплате:</span>
                                        <span className="total-sum-amount">{totalAmount} UAH</span>
                                     </div> }
                {!cartList.length && <div className='empty-cart'>Корзина пуста</div>}

               <div className="cart-buttons">
                    <Button backgroundColor='black' text='&larr; Продолжить покупки' onClick={closeCart}/>
                   {!!cartList.length && <Button backgroundColor='indianred' text='Оформить покупку &rarr;' onClick={doShopping}/>}
                   {!cartList.length && <Button  text='Оформить покупку &rarr;'/>}
               </div>

            </div>
        );
    }
}

Cart.propTypes = {
    cartList: PropTypes.array,
    closeCart: PropTypes.func,
    removeFromCart: PropTypes.func.isRequired,
    doShopping: PropTypes.func,
};

Cart.defaultProps = {
    closeCart: undefined,
    doShopping: undefined,
};


export default Cart;