import React, {Component} from 'react';
import './App.css';
import axios from 'axios';
import Header from "./components/Header/Header";
import Body from "./components/Body/Body";
import Modal from "./components/Modal/Modal";
import Footer from "./components/Footer/Footer";




class App extends Component {

    state = {
       products: [],
       favourites: [],
       modalOpen: false,
       cartList: [],
       currentProduct: null,
       cartIsOpen: false,
    };

    componentDidMount() {
       axios('/products.json')
           .then(res => {
               this.setState({products: res.data});
               });

       this.setState({favourites: JSON.parse(localStorage.getItem('favs')),
                            cartList: JSON.parse(localStorage.getItem('cartList'))
       });

    }

    render() {
        const {products, modalOpen, cartList, favourites, cartIsOpen} = this.state;
        return (
            <>
                {modalOpen && <Modal header='' closeButton text='Добавить товар в корзину? '
                                     onClick={this.closeModal} actionBtnClassName='btn--modal'
                                     confirm={this.confirmPurchase}/>}
                <Header cartList={cartList} removeFromCart={this.removeFromCart} doShopping={this.doShopping}
                        showCart={this.showCart} closeCart={this.closeCart} cartIsOpen={cartIsOpen}/>
                <Body products={products} addToFav={this.addToFav} removeFav={this.removeFav} closeCart={this.closeCart}
                      openModal={this.openModal} getCurrentProduct={this.getCurrentProduct} favourites={favourites}/>
                <Footer/>

            </>
        );

    };


    addToFav = (product) => {
        const {favourites} = this.state;
        localStorage.setItem('favs', JSON.stringify([...favourites, product]));
        this.setState({favourites: [...favourites, product]});


    };

    removeFav = (product) => {
        const {favourites} = this.state;
        this.setState({favourites: favourites.filter(element => element !== product)});
        localStorage.setItem('favs', JSON.stringify(favourites.filter(element => element !== product)));
    };

    openModal = () => {
        this.setState({modalOpen: true});
    };

    closeModal = () => {
        this.setState({modalOpen: false});
    };

    addToCart = (product) => {
        const {cartList} = this.state;

        this.setState({cartList: [...cartList, product]});
        localStorage.setItem('cartList', JSON.stringify([...cartList, product]));

    };


    confirmPurchase = () => {
        const {currentProduct, cartList} = this.state;
        this.closeModal();
        if (cartList.find(product => product.id === currentProduct.id)){
            return;
        }
        this.addToCart(currentProduct);
    };

    getCurrentProduct = (product) => {
        this.setState({currentProduct: product})
     };


    removeFromCart = (id) => {
        const {cartList} = this.state;
        this.setState({cartList: cartList.filter(item => item.id !== id)});
        localStorage.setItem('cartList', JSON.stringify(cartList.filter(item => item.id !== id)));
    };

    showCart = () => {
        this.setState({cartIsOpen: true});
    };

    closeCart = () => {
        this.setState({cartIsOpen: false});
    };

    doShopping = () => {
        this.closeCart();
        this.setState({cartList: []});
        localStorage.setItem('cartList', JSON.stringify([]));

    }

 }

export default App;
