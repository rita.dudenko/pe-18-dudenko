import React, {PureComponent} from 'react';
import './Modal.scss'

class Modal extends PureComponent {
    render() {
        const {header, closeButton, text, actions, onClick, contentClassName, btnClassName} = this.props;
        const modalContentClass = contentClassName ? `modal__content ${contentClassName}` : 'modal__content';
        const spanStyle = btnClassName ? `btn ${btnClassName}` : 'btn';


        return (
            <div className='modal' onClick={onClick}>
                <div className={modalContentClass} onClick={e => e.stopPropagation()}>
                    <div className='modal__header'>
                        <h3>{header}</h3>
                        {closeButton && <span className={spanStyle} onClick={onClick}>X</span>}
                    </div>
                        <div className='modal__text'>{text}</div>
                        {actions}
                </div>

            </div>
        );
    }
}

export default Modal;