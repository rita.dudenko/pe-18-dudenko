import React, {PureComponent} from 'react';
import './Button.scss';


class Button extends PureComponent {

    render() {
        const { backgroundColor,text, onClick, className } = this.props;
        const btnClassName = className ? `btn ${className}` : 'btn';
        return (
            <button className={btnClassName} style={{backgroundColor}} onClick={onClick}>{text}</button>

        );
    }
}

export default Button;