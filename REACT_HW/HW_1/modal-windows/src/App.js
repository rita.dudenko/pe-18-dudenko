import React, {Component} from 'react';
import './App.css';
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";


class App extends Component {
    state = {
        modalFirstOpen: false,
        modalSecondOpen: false,

    };

    render() {
        const {modalFirstOpen, modalSecondOpen} = this.state;
        const actionView = <div className='modal__actions'>
            <Button className='btn--modal' backgroundColor='rgba(0,0,0,0.2)'
                    text='Ok' onClick={this.closeModal}/>
            <Button className='btn--modal' backgroundColor='rgba(0,0,0,0.2)'
                    text='Cancel' onClick={this.closeModal}/>
        </div>;

        const actionViewBlack = <div className='modal__actions'>
            <Button className='btn--modal btn--text-black' backgroundColor='rgba(0,0,0,0.2)'
                    text='Continue' onClick={this.closeModal}/>
            <Button className='btn--modal btn--text-black' backgroundColor='rgba(0,0,0,0.2)'
                    text='Back' onClick={this.closeModal}/>
        </div>;

        return (
            <>
                {modalFirstOpen && <Modal cheader='Do you want to delete this file?' closeButton text='Once you delete this file,
           it won’t be possible to undo this action. Are you sure you want to delete it?'
                                          actions={actionView} onClick={this.closeModal}/>}

                {modalSecondOpen && <Modal contentClassName='modal__content--primary' btnClassName='btn--text-black'
                                           header='Welcome to Teams' closeButton text='You can use Zapies for Teams for free for 1 month.
                  From next month it will const 5$ per month'
                                           actions={actionViewBlack} onClick={this.closeModal}/>}

                <Button backgroundColor='red' text='Open first modal' onClick={this.openFirstModal}/>
                <Button backgroundColor='green' text='Open second modal' onClick={this.openSecondModal}/>

            </>
        );

    };

    openFirstModal = () => {
        this.setState({modalFirstOpen: true});
    };

    openSecondModal = () => {
        this.setState({modalSecondOpen: true});
    };


    closeModal = () => {
        this.setState({modalFirstOpen: false, modalSecondOpen: false});
    }
}

export default App;
