import React from 'react';
import './Cart.scss'
import Button from "../../components/Button";
import CartItem from "../../components/CartItem";
import  {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {doShoppingOperation} from '../../store/cartList/operations';

const Cart = ({cartList, doShopping}) => {

    const chosenProducts = cartList.map(item =>
        <CartItem key={item.id}  product={item}/>);

    const totalAmount = cartList.reduce((sum, product) => {
        const {price, quantity} = product;
        return sum + parseInt(price) * quantity ;
    }, 0);

    return (
        <div className='cart-container'>
            <h2 className="cart-title">Ваша покупка</h2>
            {chosenProducts}
            {!!cartList.length && <div className="total-sum">
                <span>К оплате:</span>
                <span className="total-sum-amount">{totalAmount} UAH</span>
            </div>}
            {!cartList.length && <div className='empty-cart'>Корзина пуста</div>}

            <div className="cart-buttons">
                <Link to="/"><Button backgroundColor='black' text='&larr; Продолжить покупки'/></Link>
                {!!cartList.length &&
                <Button backgroundColor='indianred' text='Оформить покупку &rarr;' onClick={doShopping}/>}
                {!cartList.length && <Button text='Оформить покупку &rarr;'/>}
            </div>

        </div>
    );

};

const mapStateToProps = ({cartList}) => ({cartList});

const mapDispatchToProps = (dispatch) => ({
    doShopping: () => dispatch(doShoppingOperation()),

});

export default connect(mapStateToProps, mapDispatchToProps)(Cart);