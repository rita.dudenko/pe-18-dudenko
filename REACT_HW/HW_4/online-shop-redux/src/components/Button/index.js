import React from 'react';
import './Button.scss';

const Button = ({backgroundColor, text, onClick, className, children}) => {

        const btnClassName = className ? `btn ${className}` : 'btn';
        return (
            <button style={{backgroundColor}} className={btnClassName}  onClick={onClick}>{text}{children}</button>

        );
};

export default Button;