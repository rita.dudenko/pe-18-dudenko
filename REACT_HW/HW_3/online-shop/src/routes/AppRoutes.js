import React from 'react';
import {Route, Switch} from 'react-router-dom';
import Cart from "../pages/Cart/Cart";
import Favourites from '../pages/Favourites/Favourites';
import Body from "../components/Body/Body";
import PropTypes from 'prop-types';



const AppRoutes = (props) => {

        const {cartList, doShopping, products, addToFav, removeFav, getCurrentProduct, openModal,
            favourites, openModalCart, addQuantity} = props;
        return (
            <>
                <Switch>
                <Route exact path='/' render={() => <Body products={products} addToFav={addToFav}
                                                          removeFav={removeFav}
                                                          getCurrentProduct={getCurrentProduct}
                                                          openModal={openModal} favourites={favourites}/>}/>
                <Route exact path='/favourites' render={() => <Favourites removeFav={removeFav} favourites={favourites}
                                                                          openModal={openModal} getCurrentProduct={getCurrentProduct} />}/>
                <Route exact path='/cart' render={() => <Cart cartList={cartList} doShopping={doShopping}
                                                              openModalCart={openModalCart}  addQuantity={addQuantity}
                                                              getCurrentProduct={getCurrentProduct}/>}/>
                </Switch>
            </>
        );

};

AppRoutes.propTypes = {
    cartList: PropTypes.array,
    doShopping: PropTypes.func,
    products: PropTypes.array.isRequired,
    addToFav: PropTypes.func,
    removeFav: PropTypes.func,
    getCurrentProduct: PropTypes.func.isRequired,
    openModal: PropTypes.func,
    favourites: PropTypes.array,
    openModalCart: PropTypes.func,
    addQuantity: PropTypes.func,
};

AppRoutes.defaultProps = {
    cartList: [],
    doShopping: undefined,
    addToFav: undefined,
    removeFav: undefined,
    openModal: undefined,
    favourites: [],
    openModalCart: undefined,
    addQuantity: undefined,
};


export default AppRoutes;