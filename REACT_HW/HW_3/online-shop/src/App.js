import React, {useEffect, useState} from 'react';
import './App.css';
import axios from 'axios';
import Header from "./components/Header/Header";
import Modal from "./components/Modal/Modal";
import Footer from "./components/Footer/Footer";
import AppRoutes from "./routes/AppRoutes";



const App = () => {

    const [products, setProducts] = useState([]);
    const [favourites, setFavourites] = useState([]);
    const [modalOpen, setModalOpen] = useState(false);
    const [cartList, setCartList] = useState([]);
    const [currentProduct, setCurrentProduct] = useState(null);
    const [cartModalOpen, setCartModalOpen] = useState(false);


    useEffect(() => {
       axios('/products.json')
           .then(res => {
               setProducts(res.data);
               });
        setFavourites(JSON.parse(localStorage.getItem('favs')));
        setCartList(JSON.parse(localStorage.getItem('cartList')));

    }, []);


    const addToFav = (product) => {
        localStorage.setItem('favs', JSON.stringify([...favourites, product]));
        setFavourites([...favourites, product]);


    };

   const removeFav = (product) => {
        setFavourites(favourites.filter(element => element.id !== product.id));
        localStorage.setItem('favs', JSON.stringify(favourites.filter(element => element.id !== product.id)));
    };

   const openModal = () => {
        setModalOpen(true);
    };

  const openModalCart = () => {
        setCartModalOpen( true);
    };

  const closeModal = () => {
        setModalOpen(false);
    };

   const closeModalCart = () => {
       setCartModalOpen(false);
    };

    const addQuantity = (product) => {
        setCartList(cartList.map(obj => obj.id === product.id ? {...obj, quantity : product.quantity} : obj ));
        localStorage.setItem('cartList', JSON.stringify(cartList
                               .map(obj => obj.id === product.id ? {...obj, quantity : product.quantity} : obj )));
    };

   const addToCart = (product) => {
        const cartElement = cartList.find(item => item.id === product.id);
        if (!!cartElement) {
            cartElement.quantity ++;
            addQuantity(cartElement);
            return;
        }
        product.quantity = 1;
        setCartList( [...cartList, product]);
        localStorage.setItem('cartList', JSON.stringify([...cartList, product]));

    };


   const confirmPurchase = () => {
        closeModal();
        addToCart(currentProduct);
    };

   const getCurrentProduct = (product) => {
        setCurrentProduct(product);
    };




   const removeFromCart = (id) => {
        setCartList(cartList.filter(item => item.id !== id));
        localStorage.setItem('cartList', JSON.stringify(cartList.filter(item => item.id !== id)));
    };


   const confirmDeletionFromCart = () => {
       removeFromCart(currentProduct.id);
        closeModalCart();
    };


   const doShopping = () => {
        setCartList( []);
        localStorage.setItem('cartList', JSON.stringify([]));

    };




    return (
            <>
                {modalOpen && <Modal header='' closeButton text='Добавить товар в корзину? '
                                     onClick={closeModal} actionBtnClassName='btn--modal'
                                     confirm={confirmPurchase}/>}

                {cartModalOpen && <Modal header='Удаление' closeButton text='Вы действительно хотите удалить товар из корзины?'
                                     onClick={closeModalCart} actionBtnClassName='btn--modal'
                                     confirm={confirmDeletionFromCart}/>}
                <Header cartList={cartList} />
                <AppRoutes cartList={cartList} removeFromCart={removeFromCart} doShopping={doShopping}
                           products={products} addToFav={addToFav} removeFav={removeFav}
                           getCurrentProduct={getCurrentProduct} openModal={openModal} openModalCart={openModalCart}
                           favourites={favourites}  addQuantity={addQuantity}/>

                <Footer/>

            </>
        );
 };

export default App;
