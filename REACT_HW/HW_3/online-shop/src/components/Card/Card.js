import React from 'react';
import "./Card.scss";
import Button from "../Button/Button";
import Icon from "../Icon/Icon";
import PropTypes from 'prop-types';

const Card = (props) => {
        const {product, favourites, addToFav, removeFav, openModal, getCurrentProduct} = props;
        const {title, price, url, article, color} = product;
        const isInFav = favourites.find(item => item.id === product.id);

   const addCardToFav = () => {
        addToFav(product);
    };

   const removeFromFav = () => {
        removeFav(product);
    };

   const addProductToCart = () => {
        openModal();
        getCurrentProduct(product);
    };


    return (
            <div className='card'>
                <div className="img-wrapper"><img src={url} alt='flower'/></div>
                <h5 className="product-title">{title}</h5>
                <p className="product-article">Артикул: {article}</p>
                <p className="product-color">Цвет: {color}</p>
                <h5 className="price">{price}
                    {!isInFav && <Icon className="fa fa-star-o icon--m-l-30px" onClick={addCardToFav}/>}
                    {!!isInFav && <Icon className="fa fa-star icon--gold icon--m-l-30px" onClick={removeFromFav}/>}
                </h5>
                <div className='button-wrapper'>
                    <Button backgroundColor='indianred' className='btn--card' text="Купить"
                            onClick={addProductToCart}/>
                </div>
            </div>
        );
    };


Card.propTypes = {
    product: PropTypes.shape({
        id: PropTypes.number.isRequired,
        title: PropTypes.string.isRequired,
        price: PropTypes.string.isRequired,
    }).isRequired,
    favourites: PropTypes.array,
    addToFav: PropTypes.func,
    removeFav: PropTypes.func,
    openModal: PropTypes.func.isRequired,
    getCurrentProduct: PropTypes.func.isRequired,
};

Card.defaultProps = {
    addToFav: undefined,
    removeFav: undefined,
};

export default Card;