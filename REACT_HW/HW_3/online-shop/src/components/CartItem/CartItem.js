import React from 'react';
import Button from "../Button/Button";
import Icon from "../Icon/Icon";
import "./CartItem.scss";
import PropTypes from 'prop-types';

const CartItem = (props) => {
    const {product, openModalCart, getCurrentProduct, addQuantity} = props;
    const {id, title, url, price, quantity} = product;

    const deleteProduct = () => {
        openModalCart();
        getCurrentProduct(product);

    };

    const decProductQuantity = () => {
        if (quantity > 1){
            product.quantity = quantity - 1;
        }
        addQuantity(product);
    };

    const incProductQuantity = () => {
        product.quantity = quantity + 1;
        addQuantity(product);
    };


    return (
        <div className='product-cart-view' key={id}>
            <span><img className='cart-img' src={url} alt='flower'/></span>
            <span className='product-cart-title'>{title}</span>
            <span>
                       <Button backgroundColor="black" text="--" onClick={decProductQuantity}/>
                       <span className="product-cart-quantity">{quantity}</span>
                       <Button backgroundColor="black" text="+" onClick={incProductQuantity}/>
                   </span>
            <span className='product-cart-price'>{price}</span>
            <span className='delete-product' onClick={deleteProduct}><Icon
                className='fa fa-times'/></span>
        </div>
    );

};

CartItem.propTypes = {
    product: PropTypes.shape({
        id: PropTypes.number.isRequired,
        title: PropTypes.string.isRequired,
        price: PropTypes.string.isRequired,
        quantity: PropTypes.number.isRequired,
    }).isRequired,
    openModalCart: PropTypes.func,
    getCurrentProduct: PropTypes.func.isRequired,
    addQuantity: PropTypes.func,

};

CartItem.defaultProps = {
    openModalCart: undefined,
    addQuantity: undefined,
};


export default CartItem;