import React from 'react';
import './Favourites.scss';
import Button from "../../components/Button/Button";
import {withRouter} from 'react-router-dom';
import Card from "../../components/Card/Card";
import PropTypes from 'prop-types';


const Favourites = (props) => {

        const {favourites, removeFav, openModal, getCurrentProduct} = props;

        const favProducts = favourites.map(item => {
            return(
                <Card key={item.id} product={item} removeFav={removeFav} favourites={favourites}
                      openModal={openModal} getCurrentProduct={getCurrentProduct} />
            )
        });

   const closeFavourites = () => {
        props.history.push('/');
    };

        return (
            <div className='favourites-container'>
                <h2 className="favourites-title">Избранное</h2>
                <div className="fav-cards-container">{favProducts}</div>
                <div className="fav-buttons">
                    <Button backgroundColor='black' text='&larr; Назад' onClick={closeFavourites}/>
                </div>

            </div>
        );
};


Favourites.propTypes = {
    favourites: PropTypes.array,
    removeFav: PropTypes.func,
    openModal: PropTypes.func,
    getCurrentProduct: PropTypes.func.isRequired,
};

Favourites.defaultProps = {
    removeFav: undefined,
    openModal: undefined,
};


export default withRouter(Favourites);

