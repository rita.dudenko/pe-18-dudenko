import React from 'react';
import './Cart.scss'
import Button from "../../components/Button/Button";
import CartItem from "../../components/CartItem/CartItem";
import PropTypes from 'prop-types';
import  {Link} from 'react-router-dom';



const Cart = (props) => {

    const {cartList, doShopping, openModalCart, getCurrentProduct, addQuantity} = props;


    const chosenProducts = cartList.map(item =>
        <CartItem key={item.id}  product={item}  openModalCart={openModalCart} getCurrentProduct={getCurrentProduct}
                  addQuantity={addQuantity}/>);

    const totalAmount = cartList.reduce((sum, product) => {
        const {price, quantity} = product;
        return sum + parseInt(price) * quantity ;
    }, 0);


    return (
        <div className='cart-container'>
            <h2 className="cart-title">Ваша покупка</h2>
            {chosenProducts}
            {!!cartList.length && <div className="total-sum">
                <span>К оплате:</span>
                <span className="total-sum-amount">{totalAmount} UAH</span>
            </div>}
            {!cartList.length && <div className='empty-cart'>Корзина пуста</div>}

            <div className="cart-buttons">
                <Link to="/"><Button backgroundColor='black' text='&larr; Продолжить покупки'/></Link>
                {!!cartList.length &&
                <Button backgroundColor='indianred' text='Оформить покупку &rarr;' onClick={doShopping}/>}
                {!cartList.length && <Button text='Оформить покупку &rarr;'/>}
            </div>

        </div>
    );

};


Cart.propTypes = {
    cartList: PropTypes.array,
    doShopping: PropTypes.func,
    openModalCart: PropTypes.func,
    getCurrentProduct: PropTypes.func.isRequired,
    addQuantity: PropTypes.func,

};

Cart.defaultProps = {
    openModalCart: undefined,
    addQuantity: undefined,
    doShopping: undefined,
};


export default Cart;